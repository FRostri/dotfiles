local wezterm = require "wezterm"

local config = {}

-- wezterm.action.SpawnCommandInNewWindow {
--   label = "List all the files!",
--   args = {"ls", "-al"}
-- }

if wezterm.config_builder then
  config = wezterm.config_builder()
end

config.font = wezterm.font "FiraCode NFM"
config.color_scheme = "tokyonight_night"

config.keys = {
  { key = "l", mods = "ALT", action = wezterm.action.ShowLauncher },
  -- {
  --   key = "p",
  --   mods = "ALT",
  --   action = wezterm.action.SpawnCommandInNewTab {
  --     label = "List all the files!",
  --     args = { "Arch.exe" }
  --   }
  -- }
}

return config
