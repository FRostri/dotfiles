# PATH
export PATH=$HOME/.fnm:$PATH
export PATH=/usr/bin/lua-language-server/bin:$PATH
export PATH=$HOME/go/bin:$PATH
export PATH=$HOME/.local/bin:$PATH
export PATH=$HOME/.cargo/bin:$PATH
export PATH=$HOME/go/bin:$PATH
export PATH=$HOME/.bun/bin:$PATH
export PATH="$HOME/.fnm/node-versions/v16.18.0/installation/bin":$PATH
export PATH=/var/lib/snapd/snap/bin:$PATH

export GPG_TTY=$(tty)
export EDITOR=lvim
export OPENSSL_CONF=/etc/ssl
export JAVA_HOME=/opt/android-studio/jre
export ANDROID_HOME=$HOME/Android/Sdk
export NDK_HOME="$ANDROID_HOME/ndk/25.1.8937393"

alias ls="exa -g --icons"
alias la="ls -a"
alias ll="ls -l"
alias lla="ll -a"
alias luamake=/usr/bin/lua-language-server/3rd/luamake/luamake
alias v=lvim
alias gst="git status"
alias gcm="git commit -S"
alias gcma="git commit -S -a"
alias gad="git add -A"
alias gps="git push"
alias gpl="git pull"
alias glg="git lg"
alias cntr="cargo nextest run"
alias cntrc="cargo nextest run --no-capture"
alias pls="sudo"
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias cat="bat"
# alias code="/mnt/c/Users/zackf/Microsoft\ VS\ Code/bin/code"

bindkey "^[[1;3C" forward-word
bindkey "^[[1;3D" backward-word
eval "$(starship init zsh)"

export PATH="$PATH:/home/isaac/.bin"

zsh_plugins=${ZDOTDIR:-$HOME}/.zsh_plugins
if [[ ! ${zsh_plugins}.zsh -nt ${zsh_plugins}.txt ]]; then
  (
    source /usr/share/zsh-antidote/antidote.zsh
    antidote bundle <${zsh_plugins}.txt >${zsh_plugins}.zsh
  )
fi
source ${zsh_plugins}.zsh

# fnm
export PATH="/home/zackf/.local/share/fnm:$PATH"
eval "`fnm env`"
