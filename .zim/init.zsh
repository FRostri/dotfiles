zimfw() { source /home/isaac/.zim/zimfw.zsh "${@}" }
zmodule() { source /home/isaac/.zim/zimfw.zsh "${@}" }
fpath=(/home/isaac/.zim/modules/git/functions /home/isaac/.zim/modules/utility/functions /home/isaac/.zim/modules/duration-info/functions /home/isaac/.zim/modules/git-info/functions /home/isaac/.zim/modules/prompt-pwd/functions /home/isaac/.zim/modules/zsh-completions/src ${fpath})
autoload -Uz -- git-alias-lookup git-branch-current git-branch-delete-interactive git-branch-remote-tracking git-dir git-ignore-add git-root git-stash-clear-interactive git-stash-recover git-submodule-move git-submodule-remove mkcd mkpw duration-info-precmd duration-info-preexec coalesce git-action git-info prompt-pwd
source /home/isaac/.zim/modules/environment/init.zsh
source /home/isaac/.zim/modules/git/init.zsh
source /home/isaac/.zim/modules/input/init.zsh
source /home/isaac/.zim/modules/termtitle/init.zsh
source /home/isaac/.zim/modules/utility/init.zsh
source /home/isaac/.zim/modules/duration-info/init.zsh
source /home/isaac/.zim/modules/sorin/sorin.zsh-theme
source /home/isaac/.zim/modules/powerlevel10k/powerlevel10k.zsh-theme
source /home/isaac/.zim/modules/completion/init.zsh
source /home/isaac/.zim/modules/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /home/isaac/.zim/modules/zsh-history-substring-search/zsh-history-substring-search.zsh
source /home/isaac/.zim/modules/zsh-autosuggestions/zsh-autosuggestions.zsh
