Config { 
    font = "xft:UbuntuMono Nerd Font:weight=bold:pixelsize=12:antialias=true:hinting=true",
    bgColor = "#1a212e",
    fgColor = "#93a4c3",
    lowerOnStart = True,
    hideOnStart = False,
    allDesktops = True,
    persistent = True,
    commands = [ 
        Run Date "  %d %b %Y %H:%M " "date" 600,
        Run Com "bash" ["-c", "checkupdates | wc -l"] "updates" 3000,
        Run UnsafeStdinReader
    ],
    sepChar = "%",
    alignSep = "}{",
    template = "<fc=#862aa1>   </fc> %UnsafeStdinReader%"
}
