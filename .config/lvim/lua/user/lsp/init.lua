require("user.lsp.languages.rust")
require("user.lsp.languages.js-ts")
require("user.lsp.languages.python")

lvim.format_on_save = false
lvim.lsp.diagnostics.virtual_text = true

lvim.builtin.treesitter.ensure_installed = {
  "rust",
  "python",
  "javascript",
  "astro"
}

-- vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "pyright", "pylsp" })
-- lvim.lsp.automatic_configuration.skipped_servers = vim.tbl_filter(function(server)
--   return server ~= "pylsp"
-- end, lvim.lsp.automatic_configuration.skipped_servers)

local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
  { command = "stylua", fyletypes = { "lua" } },
  {
    command = "prettier",
    fyletypes = {
      "javascript",
      "astro",
      "typescript",
      "tsx",
      "json",
      "jsonc",
      "css",
      "html"
    }
  },
  { command = "black", fyletypes = { "python" } },
  { command = "rustfmt", fyletypes = { "rust" } }
}
